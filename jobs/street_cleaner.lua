-- Street Cleaner job

-- Vehicles (re)spawning table
-- Structure = vehicle object (default: nil) | xcoord | y | z | z-rotation
local street_cleaner_vehicles = {
	{nil, 159.0, -22.0, 1.55, 270.0},
	{nil, 171.0, -48.0, 1.55, 270.0},
	{nil, 171.0, -45.0, 1.55, 270.0},
	{nil, 171.0, -42.0, 1.55, 270.0},
	{nil, 171.0, -39.0, 1.55, 270.0},
	{nil, 171.0, -36.0, 1.55, 270.0},
	{nil, 1670.5, -1884.0, 13.5, 0},
	{nil, 1668, -1884.0, 13.5, 0},
	{nil, 1665.5, -1884.0, 13.5, 0},
	{nil, 1663, -1884.0, 13.5, 0},
	{nil, 1670.5, -1890.0, 13.5, 180},
	{nil, 1668, -1890.0, 13.5, 180},
	{nil, 1665.5, -1890.0, 13.5, 180},
	{nil, 1663, -1890.0, 13.5, 180},
}

-- The list of players currently running the job
-- Structure = { theplayer, start_job_time, previous_coordinates, total_distance }
local current_running_jobs = {}

-- Constants
local MAX_UNOCCUPIED_VEHICLE_DISTANCE_FROM_SPAWN = 10.0 -- empty vehicles further than 10 meters from spawn are respawned
local PAYMENT_PER_DISTANCE_DONE = 0.020 * get('JOBS_EARNING_MULTIPLIER') -- x$ per meter cleaned
local PAYMENT_PER_MINUTE_OF_WORK = 0.015 * get('JOBS_EARNING_MULTIPLIER') -- Tune later
local MINIMUM_TIME_TO_GET_PAID = 25.0 -- seconds
local MINIMUM_DISTANCE_TO_GET_PAID = 100.0 -- meters
local SWEEPER_VEHICLE_ID = 574

function respawn_street_cleaner_vehicles(src, cmd)
	cmd = cmd or nil
	local force = false
	if cmd ~= nil then 
		outputChatBox('(( Street cleaners forcefully respawned. (Sorry for inconvenience) ))')
		force = true
	end

	for index, entry in ipairs(street_cleaner_vehicles) do
		local vehicle_plate = "SWEEPR" .. index

		if entry[1] == nil then
			-- spawn it
			local vehicle = createVehicle(SWEEPER_VEHICLE_ID, entry[2], entry[3], entry[4], 0, 0, entry[5], vehicle_plate)
			setVehicleRespawnPosition(vehicle, entry[2], entry[3], entry[4], 0,0, entry[5])
			entry[1] = vehicle
		--respawn it if unoccupied and not at starting place!
		elseif not isVehicleOccupied(entry[1]) or isVehicleBlown(entry[1]) or force then
			local x,_,z = getElementPosition(entry[1])
			local distance2D = getDistanceBetweenPoints2D(x, z, entry[2], entry[4])
			-- if isVehicleBlown(entry[1]) then 
				-- outputDebugString('Detected a blown up street cleaner! Exploded at position: ' .. x .. " | " .. z .. " -> " .. distance2D .. " meters away from its starting point.") 
			-- end
			if distance2D > MAX_UNOCCUPIED_VEHICLE_DISTANCE_FROM_SPAWN or isVehicleBlown(entry[1]) or force then
				-- Re-park it
				respawnVehicle(entry[1])
			end
		end
	end
end
-- The respawn timer + spawn at resource start
addEventHandler('onResourceStart', root, respawn_street_cleaner_vehicles)
addCommandHandler('respawn_street_cleaner', respawn_street_cleaner_vehicles, true, false)
local respawnTimer = setTimer(respawn_street_cleaner_vehicles, get('TIME_BETWEEN_JOB_VEHICLES_RESPAWN_CHECK'), 0)


function update_street_cleaner_distance_done()
	for index, running in ipairs(current_running_jobs) do
		local x,_,z = getElementPosition(running['player'])
		local distance2D = getDistanceBetweenPoints2D(x, z, running['previous_coordinates'][1], running['previous_coordinates'][2])
		running['total_distance'] = running['total_distance'] + distance2D
		running['previous_coordinates'][1] = x
		running['previous_coordinates'][2] = z
	end
end
local distance_update_timer = setTimer(update_street_cleaner_distance_done, 1000, 0)


function check_street_cleaning_start_job(player, seat, jacked)
	vehicle = source
	for index, entry in ipairs(street_cleaner_vehicles) do
		if entry[1] == vehicle then
			triggerClientEvent('on_street_cleaner_job_started', player, 'street cleaner')
			-- Write the player and start time in running table
			local start_time = getRealTime()
			local x,_,z = getElementPosition(vehicle)
			local table_to_insert = {}
			table_to_insert['player'] = player
			table_to_insert['start_job_time'] = start_time
			table_to_insert['previous_coordinates'] = {x, z}
			table_to_insert['total_distance'] = 0.0
			table.insert(current_running_jobs, table_to_insert)
		end
	end
end
addEventHandler('onVehicleEnter', root, check_street_cleaning_start_job)


function check_street_cleaning_stop_job(player, seat, jacker)
	vehicle = source
	for index, entry in ipairs(street_cleaner_vehicles) do
		
		if entry[1] == vehicle then
			
			-- Find the player back from the running table
			local retrieved_start_time = nil
			local retrieved_total_distance = nil
			for index, entry in ipairs(current_running_jobs) do
				if entry['player'] == player then
					retrieved_start_time = entry['start_job_time']
					retrieved_total_distance = entry['total_distance']
					
				-- remove from table
					table.remove(current_running_jobs, index)
					break
				end
			end

		-- calculate their payment
			if retrieved_start_time ~= nil and retrieved_total_distance ~= nil then
				local current_time = getRealTime()
				local seconds_worked = 86400 * (current_time.monthday - retrieved_start_time.monthday) 
								+ 3600 * (current_time.hour - retrieved_start_time.hour) 
								+ 60 * (current_time.minute - retrieved_start_time.minute) 
								+ 1 * (current_time.second - retrieved_start_time.second)

			-- Punish the lazy
				if retrieved_total_distance < MINIMUM_DISTANCE_TO_GET_PAID or seconds_worked < MINIMUM_TIME_TO_GET_PAID
				then
					triggerClientEvent('on_street_cleaner_job_stopped', player, "You stopped the street cleaner job, but you weren't paid because you didn't move!")
					break
				end
				
			-- gief muny
				local pre_grand_total = (seconds_worked / 60) * PAYMENT_PER_MINUTE_OF_WORK -- payment for time
				pre_grand_total = pre_grand_total + (retrieved_total_distance * PAYMENT_PER_DISTANCE_DONE) -- payment for distance
				local grand_total = round( pre_grand_total, 0 )
				givePlayerMoney(player, grand_total)
				triggerClientEvent('on_street_cleaner_job_stopped', player, "You earned $" .. grand_total .. " for cleaning " .. round(retrieved_total_distance, 2) .. " meters of street during " .. seconds_worked .. " seconds!")
			end

			break -- Do not loop more
		end

	end
end
addEventHandler('onVehicleExit', root, check_street_cleaning_stop_job)