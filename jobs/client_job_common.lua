function start_job(job_name)
	outputChatBox('You started the ' .. job_name .. ' job. (Under development)')
end
addEvent('on_street_cleaner_job_started', true)
addEventHandler('on_street_cleaner_job_started', root, start_job)


function stop_job(message)
	message = message or 'You stopped working.'
	outputChatBox(message)
end
addEvent('on_street_cleaner_job_stopped', true)
addEventHandler('on_street_cleaner_job_stopped', root, stop_job)