-- Pizzaboy spawning table
-- Structure = vehicle element, x, y, z, rotation°
local pizzaboys = {
	{nil, 2125, -1821, 13.5, 296},
	{nil, 2125, -1818, 13.5, 296},
	{nil, 2125, -1815, 13.5, 296},
	{nil, 2125, -1812, 13.5, 296},
	{nil, 2125, -1809, 13.5, 296},
	{nil, 2125, -1806, 13.5, 296},
}

-- Delivery checkpoints table

-- Running jobs table

-- vars
local PIZZABOY_VEHICLE_ID = 448
local MAX_DISTANCE_FROM_STARTING_POINT = 10.0 -- meters

	-- FUNCTIONS
-- Vehicles respawn
function respawn_pizza_vehicles(src, cmd)
	src = src or source
	cmd = cmd or false
	local force = false
	if cmd ~= false then force = true end
	

	for index, data in ipairs(pizzaboys) do
		-- If the vehicle doesn't exist, create and store it
		if data[1] == nil then
			local newPizzaboy = createVehicle(PIZZABOY_VEHICLE_ID, data[2], data[3], data[4], 0,0, data[5], "PizzaDlvr")
			setVehicleRespawnPosition(newPizzaboy, data[2], data[3], data[4], 0,0, data[5])
			data[1] = newPizzaboy
		-- If the vehicle exists
		elseif data[1] ~= nil then
			-- Check if occupied
			if not isVehicleOccupied(data[1]) then
				-- If not occupied, check distance from starting point
				local x,_,z = getElementPosition(data[1])
				local dist = getDistanceBetweenPoints2D(x, z, data[2], data[4])
				if dist >= MAX_DISTANCE_FROM_STARTING_POINT or isVehicleBlown(data[1]) or force then
					respawnVehicle(data[1])
				end
			end
		end
	end
end
addCommandHandler('respawn_pizza', respawn_pizza_vehicles, true, false)
addEventHandler('onResourceStart', root, respawn_pizza_vehicles)
local pizza_respawn_timer = setTimer(respawn_pizza_vehicles, get('TIME_BETWEEN_JOB_VEHICLES_RESPAWN_CHECK'), 0)

-- Job start signal
function check_pizzaboy_entered(player, seat, jacked)
	local car = source
	for i, v in ipairs(pizzaboys) do
		if v == var then
			outputChatBox('This job is unimplemented yet.', player)
			break
		end
	end
end
addEventHandler('onVehicleEnter', root, check_pizzaboy_entered)

-- Job end signal

-- Query new destination signal
