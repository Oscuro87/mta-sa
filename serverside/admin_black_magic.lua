-- Admin commands

-- ============= ADMIN TELEPORTATION =============
local TELEPORT_TABLE_KEY = "tps"

-- Teleport table structure = given_name, x, y, z, rotation°, skin_id, interior, dimension
function add_admin_teleport_point(player, cmd, teleport_given_name)
	teleport_given_name = teleport_given_name or nil
	
	if teleport_given_name == nil then
		outputChatBox('Wrong command, usage is settp [teleport_custom_name]', player, 255, 0, 0)
		return
	end

	local x,y,z = getElementPosition(player)
	local _,_,r = getElementRotation(player)
	local skin_id = getElementModel(player)
	local interior = getElementInterior(player)
	local dimension = getElementDimension(player)

	local entry = {teleport_given_name, x, y, z, r, skin_id, interior, dimension}
	local saved_teleports = getElementData(player, TELEPORT_TABLE_KEY)

	if not saved_teleports then
		saved_teleports = {}
	end

	for index, tp in ipairs(saved_teleports) do
		if tp[1] == teleport_given_name then
			outputChatBox('A teleport with the same name already exists.', player, 255, 0, 0)
			return
		end
	end

	table.insert(saved_teleports, entry)
	setElementData(player, TELEPORT_TABLE_KEY, saved_teleports)
	outputChatBox('Added teleport point ' .. teleport_given_name .. ' @ ' .. x .. ', ' .. y .. ', ' .. z .. ', ' .. r, player, 0, 255, 0)
end
addCommandHandler('settp', add_admin_teleport_point, true, false)


function teleport_admin_to(player, cmd, teleport_name)
	teleport_name = teleport_name or false
	if not teleport_name then
		outputChatBox('Wrong command, usage is tp [your_teleport_name]', player, 255, 0, 0)
		return
	end

	local teleports = getElementData(player, TELEPORT_TABLE_KEY)
	if not teleports then
		teleports = {}
		setElementData(player, TELEPORT_TABLE_KEY, teleports)
		outputChatBox('You don\'t have any teleport going by this name.', player, 255, 0, 0)
		return;
	end

	local theTP = false
	for index, tp in ipairs(teleports) do
		if tp[1] == teleport_name then
			theTP = tp
			break
		end
	end	

	if theTP then
		fadeCamera(player, false, 0.2)
		setTimer(fadeCamera, 401, 1, player, true, 0.2)
		setTimer(setCameraTarget, 401, 1, player, player)
		setTimer(spawnPlayer, 201, 1, player, theTP[2], theTP[3], theTP[4], theTP[5], theTP[6], theTP[7], theTP[8])
		outputChatBox('Teleport success.', player, 0, 255, 0)
	else
		outputChatBox('You don\'t have any teleport going by this name.', player, 255, 0, 0)
		return;
	end
end
addCommandHandler('tp', teleport_admin_to, true, false)


function delete_admin_tp_point(player, cmd, teleport_name)
	teleport_name = teleport_name or false
	if teleport_name == false then
		outputChatBox('Wrong command, usage is deltp [teleport_name_to_delete].', player, 255, 0, 0)
		return
	end

	local teleports = getElementData(player, TELEPORT_TABLE_KEY)
	if not teleports then
		teleports = {}
		setElementData(player, TELEPORT_TABLE_KEY, teleports)
	end

	for i, v in ipairs(teleports) do
		if v[1] == teleport_name then
			table.remove(teleports, i)
			setElementData(player, TELEPORT_TABLE_KEY, teleports)
			break
		end
	end

	outputChatBox('Teleport successfully deleted.', player, 0, 255, 0)
end
addCommandHandler('deltp', delete_admin_tp_point, true, false)

function teleport_admin_to_interior(player, cmd, interior, x, y, z, dimension)
	interior = interior or false
	x = x or false
	y = y or false
	z = z or false
	dimension = dimension or 0

	if not x or not y or not z or not interior then
		outputChatBox('Wrong command, usage is /tptointerior [interior_id] [x] [y] [z] [(optional) dimension].', player, 255, 0, 0)
		return
	end

	setElementInterior(player, interior, x, y, z)
	setElementDimension(dimension)

	outputChatBox('Teleport successful.', player, 0, 255, 0)
end
addCommandHandler('tptointerior', teleport_admin_to_interior, true, false)

-- ============= END ADMIN TELEPORTATION =============



-- ============= Admin spawn vehicle by name =============
local admin_spawned_vehicle_table = {}

function spawn_vehicle_for_admin(player, cmd, ...)
	local vehicle_name = table.concat({...}, " ") or ""
	local model_id = getVehicleModelFromName(vehicle_name)
	if not model_id then
		outputChatBox('This vehicle name doesn\'t exist.', player, 255, 0, 0)
		return
	end

	local x,y,z = getElementPosition(player)
	local _,_,r = getElementRotation(player)
	local car = createVehicle(model_id, x, y, z, 0, 0, r, "A D M I N")
	table.insert(admin_spawned_vehicle_table, car)
	warpPedIntoVehicle(player, car, 0)
	outputChatBox('Vehicle successfully spawned.', player, 0, 255, 0)
end
addCommandHandler('getveh', spawn_vehicle_for_admin, true, false)

function destroy_admin_vehicle(player, cmd)
	local car = getPedOccupiedVehicle(player)
	if not car then return end

	for i, v in ipairs(admin_spawned_vehicle_table) do
		if v == car then
			table.remove(admin_spawned_vehicle_table, i)
			break
		end
	end
	destroyElement(car)
	outputChatBox('Vehicle destroyed.', player, 255, 0, 0)
end
addCommandHandler('delveh', destroy_admin_vehicle, true, false)

function set_admin_spawned_vehicles_spawn_location()
	for index, vehicle in ipairs(admin_spawned_vehicle_table) do
		x,y,z = getElementPosition(vehicle)
		rx,ry,rz = getElementRotation(vehicle)
		setVehicleRespawnPosition(vehicle, x, y, z, rx, ry, rz)
	end
end
addEventHandler('onResourceStop', root, set_admin_spawned_vehicles_spawn_location)

function respawn_admin_spawned_vehicles()
	for i, veh in ipairs(admin_spawned_vehicle_table) do
		respawnVehicle(veh)
	end
end
addEventHandler('onResourceStart', root, respawn_admin_spawned_vehicles)

function lookup_vehicle_name_by_model(player, cmd, model)
	model = model or false
	local name = getVehicleNameFromModel(model)
	if not name or not model then
		outputChatBox('No vehicle for this model #.', player, 255, 0, 0)
		return
	end
	outputChatBox('Vehicle name: ' .. name, player, 0, 255, 0)
end
addCommandHandler('lookupvehiclename', lookup_vehicle_name_by_model, true, false)
-- ============= END ADMIN SPAWN VEHICLE BY NAME =============