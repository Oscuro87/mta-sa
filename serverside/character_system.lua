local blueberry_spawn_pos = {245.91766357422, -53.830345153809, 1.5776442289352, 271.26159667969} -- Default spawn position if no info in DB

function greetings()
  	outputChatBox("Welcome, " .. getPlayerName(source) .. "!", source)
  	outputChatBox("Please /plogin [username] [pass] OR /pregister [username] [pass] [repeat-pass] first!", source, 255, 0, 0)

  	-- Hud components
	setPlayerHudComponentVisible(source, "money", true)
end
addEventHandler("onPlayerJoin", root, greetings)



function __save_player(id, x, y, z, rotation, money, logged_in)
	dbExec(db, 'UPDATE player SET pos_x=?, pos_y=?, pos_z=?, rotation=?, money=?, logged_in=? WHERE id=?', x, y, z, r, money, logged_in, id)
	outputDebugString('Saved player state for ID ' .. id)
end



-- Kicking characters will save them in DB
function save_all_logged_characters(resource)
	root = source
	local list = getElementsByType('player')
	for index, player in ipairs(list) do
		outputChatBox("Server is restarting...", player, 255, 0, 0)
		local x,y,z = getElementPosition(player)
		local _,_,r = getElementRotation(player)
		local id = getElementData(player, 'player_id')
		local money = getPlayerMoney(player)
		if id ~= false then
			__save_player(id, x, y, z, r, money, 1)
		end
	end
end
addEventHandler('onResourceStop', root, save_all_logged_characters)



function save_player_data(quitType, reason, sourceOfQuit)
	player = source
	if quitType == "Quit" or quitType == "Kicked" or quitType == "Timed out" or quitType == "Bad Connection" or quitType == "Unknown" then
		-- save position, money
		local x,y,z = getElementPosition(player)
		local _,_,r = getElementRotation(player)
		local id = getElementData(player, 'player_id')
		local money = getPlayerMoney(player)
		if id ~= false then -- If player is actually logged in
			__save_player(id, x, y, z, r, money, 0)
		end
	end
end
addEventHandler('onPlayerQuit', root, save_player_data)


-- query_data = player(id, username, password, money, pos_x, pos_y, pos_z, rotation, logged_in)
function load_player(query_data)
	player = source
	-- outputDebugString(query_data.pos_x)
	-- set player name
	setPlayerName(player, query_data.username)
	-- spawn player
	__spawn_the_player(player, query_data.pos_x, query_data.pos_y, query_data.pos_z, query_data.rotation)
	-- set player money
	setPlayerMoney(player, query_data.money)
	-- set element data player id
	setElementData(source, 'player_id', query_data.id)
	outputDebugString(getElementData(source, 'player_id'))
end
addEvent('on_load_player', false)
addEventHandler('on_load_player', root, load_player)


function __spawn_the_player(source, x, y, z, r)
	spawnPlayer(source, x, y, z, r)
  	setCameraTarget(source, source)
  	fadeCamera(source, true, 0.2)
end

function respawn_player_at_position()
	local player = source
	local x,y,z = getElementPosition(player)
	local _,_,r = getElementRotation(player)
	local dimension = getElementDimension(player)
	local interior = getElementInterior(player)
	local skin_id = getElementModel(player)
	spawnPlayer(player, x,y,z,r,skin_id,interior,dimension)
end
addEventHandler('onPlayerWasted', root, respawn_player_at_position)