local MIN_USERNAME_LENGTH = 5
local MAX_USERNAME_LENGTH = 22

local MIN_PASSWORD_LENGTH = 5
local MAX_PASSWORD_LENGTH = 32

function register_username(src, cmd, username, pass, repeat_pass)
	username = username or nil
	pass = pass or nil
	repeat_pass = repeat_pass or ""

	if getElementData(src, 'player_id') ~= false then
		outputChatBox('You are logged in...', src, 255, 0, 0)
		return
	end

	outputChatBox('Warning: Please do NOT register more than one account, or you WILL get banned.', src)
	outputChatBox('The registration system is under construction, sorry for inconvenience.', src)
	
-- Check username legality
	if username == nil or string.len(username) < MIN_USERNAME_LENGTH or string.len(username) > MAX_USERNAME_LENGTH then
		outputChatBox('This username is not legal to use! (min length: ' .. MIN_USERNAME_LENGTH .. ', max length: ' .. MAX_USERNAME_LENGTH .. ')', src, 255, 0, 0)
		return
	end

-- Check password legality
	if pass == nil or string.len(pass) < MIN_PASSWORD_LENGTH or string.len(pass) > MAX_PASSWORD_LENGTH then
		outputChatBox('This password is not legal to use! (min length: ' .. MIN_PASSWORD_LENGTH .. ', max length: ' .. MAX_PASSWORD_LENGTH .. ')', src, 255, 0, 0)
		return
	end

-- Check password == repeat password
	if "" .. pass ~= "" .. repeat_pass then
		outputChatBox('The password must be repeated to confirm it!', src, 255, 0, 0)
		return
	end

-- Check username availability
	local callback_data = {}
	local callback = function(qh, data)
					local answer = dbPoll(qh, 0)
					if table.size(answer) > 0 then
						outputChatBox('Checking username availability... Username already taken!', src, 255, 0, 0)
						return
					else
						outputChatBox('Checking username availability... OK!', src)
				-- Insert into DB
						dbExec(db, 'INSERT INTO player(username, password) VALUES(?, MD5(?), ?)', username, pass)
						outputChatBox('Your account has been created, please login with /plogin [username] [password] !', src, 0, 255, 0)
					end
				end
	dbQuery(callback, callback_data, db, 'SELECT * FROM player WHERE username=?', username)
end
addCommandHandler('pregister', register_username, false, false)
addCommandHandler('preg', register_username, false, false)



function player_login(src, cmd, username, password)
	username = username or nil
	password = password or nil

	if getElementData(src, 'player_id') ~= false then
		outputChatBox('You are already logged in!', src, 255, 0, 0)
		return
	end

	if string.len(username) < MIN_USERNAME_LENGTH or string.len(username) > MAX_USERNAME_LENGTH or username == nil
			or string.len(password) < MIN_PASSWORD_LENGTH or string.len(password) > MAX_PASSWORD_LENGTH or password == nil then
		outputChatBox('Wrong credentials entered, try again.', src, 255, 0, 0)
		return
	end

	local callback_data = {}
	local callback = function(qh, data)
						local result = dbPoll(qh, 0)
						if table.size(result) == 0 then
							outputChatBox('Wrong credentials entered, try again.', src, 0, 255, 0)
							return
						end
						
					-- If login is successfull, unfreeze the player, set name to username. (TODO, unfreeze player AND teleport to last location)
						if result[1].logged_in ~= 0 then
							outputChatBox('This account is currently in use...', src, 255, 0, 0)
						else
							outputChatBox('Login successfull.', src, 0, 255, 0)
					-- set the logged_in in db to 1
							dbExec(db, 'UPDATE player SET logged_in=? WHERE id=?', 1, result[1].id)
							triggerEvent('on_load_player', src, result[1])
						end
					end
	dbQuery(callback, callback_data, db, "SELECT * FROM player WHERE username=? and password=MD5(?)", username, password)
end
addCommandHandler('plogin', player_login, false, false)

function prevent_player_manual_rename()
	cancelEvent()
end
addEventHandler('onPlayerChangeNick', root, prevent_player_manual_rename)