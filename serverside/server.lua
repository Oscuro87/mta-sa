function setTimeAsRealWorld()
	local realTime = getRealTime()
	setTime(realTime.hour, realTime.minute)
	if get("GAME_MINUTE_DURATION") then
		setMinuteDuration(get("GAME_MINUTE_DURATION"))
	else
		outputDebugString('Couldn\'t read setting called GAME_MINUTE_DURATION!')
		setMinuteDuration(60000)
	end
end
addEventHandler("onResourceStart", getResourceRootElement(getThisResource()), setTimeAsRealWorld)
