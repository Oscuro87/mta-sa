-- Server side client commands

function tellPlayerMoney(src, cmd)
	outputChatBox('* ' .. getPlayerName(src) .. ' is checking their wallet. *', root)
	outputChatBox('(( You have $' .. getPlayerMoney(src) .. " on you. ))", src, 0, 125, 0)
end
addCommandHandler('money', tellPlayerMoney, false, false)