-- Table structure = interior full name, interior short name, x, y, z, interior_id, dimension
local interiors_table = {
	{"Middle of the world", "zero", 0, 0, 3, 0, 0, 0},
	{"Los Santos City Hall", "lsch", 246.66972351074, 108.2811050415, 1003.21875, 0, 10, 0},
}

function teleport_admin_to_custom_interior(admin, cmd, interior_short_name)
	interior_short_name = interior_short_name or false
	if not interior_short_name then
		outputChatBox('Wrong command, usage is /tptocustominterior [interior short name]', admin, 255, 0, 0)
		return
	end

	local target = false
	local target_full_name = ""
	for i, v in ipairs(interiors_table) do
		if v[2] == interior_short_name then
			target = v
			target_full_name = v[1]
			break
		end
	end

	if not target then
		outputChatBox('Cannot find a custom interior with that short name.', admin, 255, 0, 0)
		return
	end

	local player_skin = getElementModel(admin)
	fadeCamera(admin, false, 0.2)
	setTimer(fadeCamera, 401, 1, admin, true, 0.2)
	setTimer(setCameraTarget, 401, 1, admin, admin)
	setTimer(spawnPlayer, 201, 1, admin, target[3], target[4], target[5], target[6], player_skin, target[7], target[8])

	outputChatBox('Successful teleport to ' .. target_full_name .. '.', admin, 0, 255, 0)
end
addCommandHandler('tptocustominterior', teleport_admin_to_custom_interior, true, false)