--[[
Cars table structure =
	{
		["sold_type_name"] =
		{
			{"Car full name", "minimum price", "maximum price"}
		}
	}
]]
local available_cars_table = {
	["cheap"] = {
		{"Clover", 5000, 7500},
		{"Glendale", 8000, 9999},
		{"Tampa", 5500, 8000},
	}
}

--[[ 
Car dealership spawns structure = 
	{
		["dealership_full_name"] = 
		{
			["type_sold"] = (see available_cars_table above),
			["spawn_points"] =
			{
				{vehicle_element, x, y, z, rotation, interior, dimension},
				...
			}
		},
		...
	}
]]
local dealership_table = {
	["Coutt and Schutz"] = {
		["type_sold"] = "cheap",
		["spawn_points"] = {
			{false, 2118.1472167969, -1155.6287841797, 23.615951538086, 356.35913085938, 0, 0},
			{false, 2134.9936523438, -1147.8292236328, 23.891304016113, 90.549270629883, 0, 0},
			{false, 2135.3837890625, -1143.0954589844, 24.412090301514, 89.057037353516, 0, 0},
			{false, 2134.9689941406, -1129.0832519531, 25.034803390503, 40.214851379395, 0, 0},
			{false, 2135.9716796875, -1138.4987792969, 24.915269851685, 86.143363952637, 0, 0},
			{false, 2117.9514160156, -1126.2750244141, 24.702175140381, 358.63082885742, 0, 0},
			{false, 2117.615234375, -1135.0841064453, 25.178512573242, 358.82662963867, 0, 0},
			{false, 2117.2587890625, -1142.083984375, 24.928134918213, 355.75592041016, 0, 0},
			
		}
	},
}

local TIME_BETWEEN_VEHICLE_DEALERSHIP_REFRESH = 60000
local DATA_PRICE_TAG_KEY = "price_tag" -- Name of the key for the price in getElementData(vehicle) (only for unsold vehicles)

--[[============================ FUNCTIONS ============================]]

function respawn_dealerships_cars()
	for dealership, data in pairs(dealership_table) do
		local type_sold = data.type_sold
		for index, spawn_point in ipairs(data.spawn_points) do
			if spawn_point[1] ~= false then
				destroyElement(spawn_point[1])
				spawn_point[1] = false
			end

			local vehicle_node = __pick_random_vehicle_by_type(type_sold)
			local model_id = getVehicleModelFromName(vehicle_node[1])
			local vehicle = createVehicle(model_id, spawn_point[2], spawn_point[3], spawn_point[4]+0.1, 0, 0, spawn_point[5])
			setElementInterior(vehicle, spawn_point[6])
			setElementDimension(vehicle, spawn_point[7])
			setVehicleDamageProof(vehicle, true)
			setTimer(setElementFrozen, 2000, 1, vehicle, true) -- After 500 ms so it falls on the ground first!
			spawn_point[1] = vehicle
			setElementData(vehicle, "price_tag", math.random(vehicle_node[2], vehicle_node[3]))
		end
	end
end
addEventHandler('onResourceStart', root, respawn_dealerships_cars)
local dealerships_respawn_timer = setTimer(respawn_dealerships_cars, TIME_BETWEEN_VEHICLE_DEALERSHIP_REFRESH, 0)

function __pick_random_vehicle_by_type(type_sold)
	type_sold = type_sold or false
	if not type_sold then
		outputDebugString('Cannot find type_sold "' .. type_sold .. '" in car dealership!')
		return false
	end

	local node = available_cars_table[type_sold]
	return node[math.random(1, table.size(node))]
end

function __find_spawn_point_by_vehicle_element(vehicle_element)
	for dealership, data in pairs(dealership_table) do
		for index, spawn_point in ipairs(data.spawn_points) do
			if spawn_point[1] == vehicle_element then
				return spawn_point
			end
		end
	end
	return false
end

function car_dealership_offer_to_player(player, seat, jacked)
	vehicle = source
	
	if __find_spawn_point_by_vehicle_element(vehicle) ~= false then
		outputChatBox('This vehicle is for sale! Its price is $' .. getElementData(vehicle, DATA_PRICE_TAG_KEY), player)
		outputChatBox('(( Type /buyvehicle (case insensitive) to buy. ))', player, 0, 255, 255)
	end
end
addEventHandler('onVehicleEnter', root, car_dealership_offer_to_player)

function car_dealership_player_buy_vehicle(player, cmd)
	local vehicle_element = getPedOccupiedVehicle(player)
	if not vehicle_element then return end

	local spawn_point = __find_spawn_point_by_vehicle_element(vehicle_element)
	if not spawn_point then return end

	local price = getElementData(vehicle_element, DATA_PRICE_TAG_KEY)
	if price > getPlayerMoney(player) then
		outputChatBox('You do not have enough money to buy this vehicle.', player, 255, 0, 0)
		return
	end

	-- TODO: remove the price tag + unfreeze the car + remove damage proof + remove the vehicle element from spawn point (set to false) + set owner id
end
addCommandHandler('buyvehicle', car_dealership_player_buy_vehicle)