-- Client side client commands

function printPlayerCoords(cmd)
	local x,y,z = getElementPosition(localPlayer)
	local _,_,r = getElementRotation(localPlayer)
	local interior = getElementInterior(localPlayer)
	local dimension = getElementDimension(localPlayer)
	setClipboard(x .. ", " .. y .. ", " .. z .. ", " .. r .. ", " .. interior .. ", " .. dimension)
	outputChatBox("(( Coordinates = x, y, z, rotation, interior, dimension ))")
	outputChatBox("(( Your coordinates are: " .. x .. ", " .. y .. ", " .. z .. ", " .. r .. ", " .. interior .. ", " .. dimension .. " (Copied to clipboard!) ))")
end
addCommandHandler("coords", printPlayerCoords, true)